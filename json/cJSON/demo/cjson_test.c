#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"

int main(int argc, char **argv)
{
    //创建空JSON
    /*创建{}。cJSON_CreateObject执行后，会申请一块内存，使用完毕后，需要手动释放*/
    cJSON *monitor = cJSON_CreateObject();

    //对创建好的JSON进行格式化打印。cJSON_Print执行后会提请一块内存，使用完毕后，需要手动释放
    char *string = cJSON_Print(monitor);
    if (string == NULL)
    {
        fprintf(stderr, "Failed to print monitor.\n");
    }

    printf("%s\n", string);


    //解析空JSON
	cJSON *monitor_json = cJSON_Parse(string);
	if (monitor_json == NULL)
    {
        printf("err\n");
    }
end:
    free(string);
    cJSON_Delete(monitor);
    cJSON_Delete(monitor_json);
    return 0;
}
