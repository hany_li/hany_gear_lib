#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "mem_ops.h"

static void panic(const char *fmt, ...);
static arg_panicfn *s_panic = panic;

void dbg_printf(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}

static void panic(const char *fmt, ...)
{
    va_list args;
    char *s;

    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4996)
#endif
    s = getenv("EF_DUMPCORE");
#if defined(_MSC_VER)
#pragma warning(pop)
#endif

    if (s != NULL && *s != '\0')
    {
        abort();
    }
    else
    {
        exit(EXIT_FAILURE);
    }
}

void arg_set_panic(arg_panicfn *proc)
{
    s_panic = proc;
}

void *xmalloc(size_t size)
{
    void *ret = malloc(size);
    if (!ret)
    {
        s_panic("Out of memory!\n");
    }
    return ret;
}

void *xcalloc(size_t count, size_t size)
{
    size_t allocated_count = count && size ? count : 1;
    size_t allocated_size = count && size ? size : 1;
    void *ret = calloc(allocated_count, allocated_size);
    if (!ret)
    {
        s_panic("Out of memory!\n");
    }
    return ret;
}

void *xrealloc(void *ptr, size_t size)
{
    size_t allocated_size = size ? size : 1;
    void *ret = realloc(ptr, allocated_size);
    if (!ret)
    {
        s_panic("Out of memory!\n");
    }
    return ret;
}

void xfree(void *ptr)
{
    free(ptr);
}

void xfree_safe(void **ptr)
{
    if (NULL != ptr || NULL != *ptr)
    {
        xfree(*ptr);
        *ptr = NULL;
    }
}

#if 1
#define debug(format, args...)  \
    {                           \
        printf(format, ##args); \
        printf("\n");           \
    }
#else
#define debug(x)
#endif

//共享内存
#include <pthread.h>
pthread_mutex_t share_lock = PTHREAD_MUTEX_INITIALIZER;

struct shared_mem
{
    int ref_cnt;
    void *buff;
    int buff_len;
};

shared_mem_t *shared_mem_write(void *buff, int buff_len, int cnt)
{
    if (!buff || buff_len <= 0 || cnt <= 0)
    {
        debug("invalid param");
        return NULL;
    }

    // create
    shared_mem_t *p_mem = (shared_mem_t *)xmalloc(sizeof(shared_mem_t));
    p_mem->buff = xmalloc(buff_len);
    p_mem->buff_len = 0;
    p_mem->ref_cnt = 0;

    // assign value
    memmove(p_mem->buff, buff, buff_len);
    p_mem->buff_len = buff_len;
    p_mem->ref_cnt = cnt;

    return p_mem;
}

//读取共享内存内容
int shared_mem_read(shared_mem_t *p_mem, void *buff, int buff_len)
{
    int ret = 0;

    if (!p_mem || !p_mem->buff || !buff || buff_len <= 0)
    {
        debug("invalid param");
        return -1;
    }

    if (buff_len < p_mem->buff_len)
    {
        debug("buff_len is small");
        return -2;
    }

    pthread_mutex_lock(&share_lock);

    if (0 == p_mem->ref_cnt)
    {
        debug("shared mem useless");
        ret = -3;
        goto END;
    }

    memmove(buff, p_mem->buff, p_mem->buff_len);
    p_mem->ref_cnt--;

END:
    pthread_mutex_unlock(&share_lock);

    return ret;
}

void shared_mem_del(shared_mem_t *p_mem)
{
    pthread_mutex_lock(&share_lock);
    if (0 == p_mem->ref_cnt)
    {
        SAFER_FREE(p_mem->buff);
        SAFER_FREE(p_mem);
        debug("shared mem delete!");
    }
    pthread_mutex_unlock(&share_lock);
}
