#pragma once

#include <stddef.h>

//basic mem ops
typedef void(arg_panicfn)(const char *fmt, ...);

extern void arg_set_panic(arg_panicfn *proc);
extern void *xmalloc(size_t size);
extern void *xcalloc(size_t count, size_t size);
extern void *xrealloc(void *ptr, size_t size);
extern void xfree(void *ptr);
extern void xfree_safe(void **ptr);

#define SAFER_FREE(p)   xfree_safe((void **)&p)

//共享内存
//仅适用于一个productor 多个consumers的情况。即一写多读的情况
typedef struct shared_mem shared_mem_t;

extern shared_mem_t *shared_mem_write(void *buff, int buff_len, int cnt);
extern int shared_mem_read(shared_mem_t *p_mem, void *buff, int buff_len);
extern void shared_mem_del(shared_mem_t *p_mem);
