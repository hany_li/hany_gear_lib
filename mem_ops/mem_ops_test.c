#include <stdio.h>
#include "mem_ops.h"

#define debug(format, args...)  \
    {                           \
        printf(format, ##args); \
        printf("\n");           \
    }

void shared_mem_test(void)
{
    int a = 6585;
    shared_mem_t *mem1;

    mem1 = shared_mem_write(&a, sizeof(a), 2);
    if (!mem1)
    {
        debug("create shared mem error");
        goto FAILED;
    }

    //正常读取 1
    int b = 0;
    int ret = shared_mem_read(mem1, &b, sizeof(b));
    if (ret)
    {
        debug("read error %d", ret);
        goto FAILED;
    }
    else
        debug("b: %d", b);

    // dst空间太小
    char c;
    ret = shared_mem_read(mem1, &c, sizeof(c));
    if (ret)
    {
        debug("read error %d", ret);
        // goto FAILED;
    }
    else
        debug("c: %d", c);

    //正常读取2
    int d;
    ret = shared_mem_read(mem1, &d, sizeof(d));
    if (ret)
    {
        debug("read error %d", ret);
        goto FAILED;
    }
    else
        debug("d: %d", d);

    //超次读取
    int e = -1;
    ret = shared_mem_read(mem1, &e, sizeof(e));
    if (ret)
    {
        debug("read error %d", ret);
        goto FAILED;
    }
    else
        debug("e: %d", e);

FAILED:
    shared_mem_del(mem1);

}

int main(int argc, char const *argv[])
{
    shared_mem_test();

    return 0;
}
