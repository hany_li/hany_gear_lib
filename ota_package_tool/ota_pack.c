#define _XOPEN_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <assert.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ota_pack.h"
#include "tcrc.h"

#define offsetof(TYPE, MEMBER) ((size_t) & ((TYPE *)0)->MEMBER)

#define DBG_U(x)                    \
    {                               \
        printf("%s:\t%u\n", #x, x); \
    }
#define DBG_S(x)                    \
    {                               \
        printf("%s:\t%s\n", #x, x); \
    }

void usage(void)
{
    printf("Options:\n");
    printf("\t-n  file name\n");
    printf("\t-v  file version\n");
    printf("\t-m  main version\n");
    printf("\t-o  output file name\n");
    printf("\t-f  force update flag: 0 or 1\n");
    printf("\t-h  show this message\n");
}

ssize_t file_get_size(const char *path)
{
    struct stat st;
    off_t size = 0;

    if (!path)
    {
        return -1;
    }

    if (stat(path, &st) < 0)
    {
        printf("%s stat error: %s\n", path, strerror(errno));
    }
    else
    {
        size = st.st_size;
    }

    return size;
}

//确保写入是小端字节序
uint32_t little_endian_uint32(uint32_t value)
{
    uint32_t formatted_value = 0;
    uint8_t *data = (uint8_t *)&formatted_value;

    data[0] = value & 0xFF;
    data[1] = (value >> 8) & 0xFF;
    data[2] = (value >> 16) & 0xFF;
    data[3] = (value >> 24) & 0xFF;

    return formatted_value;
}

int main(int argc, const char *argv[])
{
    int ret = 0;

    printf("build time : %s %s\n\n", __DATE__, __TIME__);

    char output_file[128] = {0};

    ota_pack_header_t header = {0};
    ota_pack_body_t *body = (ota_pack_body_t *)calloc(1, sizeof(ota_pack_body_t));
    assert(body != NULL);

    ota_pack_body_t body_temp = {0};
    int index_iterm_size = sizeof(body_temp) - sizeof(body_temp.data_addr);

    header.magic_num = OTA_PACK_MAGIC_NUM;
    header.body_size += index_iterm_size;

    // for (size_t i = 0; i < argc; i++)
    // {
    //     printf("%ld:%s\n", i, argv[i]);
    // }

    // caculate struct member offset
    //  int meb_offset;
    //  // int meb_offset =  ((size_t)&(((ota_pack_header_t *)0)->body_size));
    //  meb_offset = offsetof(ota_pack_header_t, body_size);
    //  printf("%ld\n", meb_offset);

    // printf("body size %d, data_addr size %d\n", sizeof(body), sizeof(body.data_addr));

    if (argc == 1)
    {
        usage();
        ret = -1;
        goto OUT;
    }

    // 1.解析参数
    int opt;
    const char *optstr = "n:v:m:o:f:h";
    int index = 0, ver_index = 0;
    while ((opt = getopt(argc, (char *const *)argv, optstr)) != -1)
    {
        switch (opt)
        {
        case 'n':
            // printf("optarg = %s, index = %d\n", optarg, index);

            // get file name with path
            strncpy(body->index[index].file_name, optarg, sizeof(body->index[index].file_name) - 1);

            //获得该文件的大小
            body->index[index].size = file_get_size(optarg);
            header.body_size += body->index[index].size;

            index++;
            break;
        case 'v':
            // printf("optarg = %s\n", optarg);
            //仅支持-n 在前，-v在后的情况
            strncpy(body->index[ver_index].ver, optarg, sizeof(body->index[ver_index].ver) - 1);
            ver_index++;
            break;
        case 'm':
            // printf("optarg = %s\n", optarg);
            strncpy(header.main_ver, optarg, sizeof(header.main_ver) - 1);
            break;
        case 'o':
            // printf("optarg = %s\n", optarg);
            strncpy(output_file, optarg, sizeof(output_file) - 1);
            break;
        case 'f':
            // printf("optarg = %s %d\n", optarg, atoi(optarg));
            header.force_flag = atoi(optarg);
            break;
        case 'h':
        default:
            usage();
            goto OUT;
            break;
        }
    }

    // 2.打包文件

    // create data zone
    body = realloc(body, header.body_size);
    assert(body != NULL);
    // body.data_addr = (char *)malloc(header.body_size);
    // assert(body.data_addr != NULL);
    char *write_addr = &(body->data_addr);

    // fill data zone from a file
    if (index > OTA_PACK_FILES_MAX_NUM)
    {
        printf("file exceeds max count: %d\n", OTA_PACK_FILES_MAX_NUM);
        ret = -2;
        goto OUT;
    }

    for (size_t id = 0; id < index; id++)
    {
        // int offset_ctr_flag = (id != 0) ? 1 : 0;
        uint32_t file_size = body->index[id].size;

        FILE *fd = fopen(body->index[id].file_name, "rb");
        assert(fd != NULL);

        size_t rd_len = fread(write_addr, 1, file_size, fd);
        assert(rd_len == file_size);

        fclose(fd);

        // update index
        //  body->index[id].offset = offsetof(ota_pack_body_t, data_addr) + file_size * offset_ctr_flag;
        body->index[id].offset = write_addr - (char *)body;
        write_addr += file_size;

        //strip path
        strncpy(body->index[id].file_name, strrchr(body->index[id].file_name, '/') + 1, sizeof(body->index[id].file_name));

        DBG_S(body->index[id].file_name);
        DBG_S(body->index[id].ver);
        DBG_U(body->index[id].size);
        DBG_U(body->index[id].offset);
        DBG_U(body->index[id].type);
        printf("\n");
    }

    // update header
    header.body_crc = CRC32((unsigned char *)body, header.body_size);

    printf("main version: %s\n", header.main_ver);
    printf("body size %d bytes\n", header.body_size);
    printf("body crc 0x%x\n", header.body_crc);
    printf("force flag: %d\n", header.force_flag);

    // format header
    header.magic_num = little_endian_uint32(header.magic_num);
    header.body_size = little_endian_uint32(header.body_size);
    header.body_crc = little_endian_uint32(header.body_crc);

    // format index iterm
    for (size_t i = 0; i < index; i++)
    {
        body->index[i].type = little_endian_uint32(body->index[i].type);
        body->index[i].offset = little_endian_uint32(body->index[i].offset);
        body->index[i].size = little_endian_uint32(body->index[i].size);
    }

    //创建输出文件
    FILE *fd = fopen(output_file, "wb");
    assert(fd != NULL);

    //写header
    int header_size = sizeof(header);
    int wr_len = fwrite(&header, 1, header_size, fd);
    assert(wr_len == header_size);

    //写body
    wr_len = fwrite(body, 1, header.body_size, fd);
    assert(wr_len == header.body_size);

    // close
    fclose(fd);

OUT:
    free(body);
    body = NULL;

    return ret;
}
