#ifndef __OTA_PACK__
#define __OTA_PACK__

#include <stdint.h>

#define OTA_PACK_MAGIC_NUM (0x20212016)

#define OTA_PACK_FILES_MAX_NUM (15)
#define OTA_PACK_FILE_NAME_LEN (128)
#define OTA_PACK_VER_LEN (32)
#define OTA_PACK_RESERVED_LEN (32)

typedef struct _ota_pack_header_
{
    uint32_t magic_num;
    uint32_t body_size;
    uint32_t body_crc;
    uint8_t force_flag;
    char main_ver[OTA_PACK_VER_LEN];
    char reserved[OTA_PACK_RESERVED_LEN];
} ota_pack_header_t;

enum
{
    FILE_TYPE_DSP = 0,
    FILE_TYPE_MCU,
    FILE_TYPE_WIFI,

    FILE_TYPE_BUTT
};

typedef struct _ota_pack_index_
{
    uint32_t type;
    uint32_t offset;
    uint32_t size;
    char file_name[OTA_PACK_FILE_NAME_LEN];
    char ver[OTA_PACK_VER_LEN];
    char reserved[OTA_PACK_RESERVED_LEN];
} ota_pack_index_t;

typedef struct _ota_pack_body_
{
    ota_pack_index_t index[OTA_PACK_FILES_MAX_NUM];
    char data_addr;
} ota_pack_body_t;

typedef struct _iterm_info_
{
    uint32_t type;
    char file_name[OTA_PACK_FILE_NAME_LEN];
    char ver[OTA_PACK_VER_LEN];
} iterm_info_t;

typedef struct _ota_unpack_info_
{
    uint8_t force_flag;
    char main_ver[OTA_PACK_VER_LEN];
    iterm_info_t iterm_info[OTA_PACK_FILES_MAX_NUM];
} ota_unpack_info_t;

/**
 * @brief 解析一个包文件
 *
 * @param file
 * @param info
 * @param fix_file_name 是否采用提前指定的文件名称
 * @return int 0：成功，others:失败
 */
int ota_unpack_file(const char *file, ota_unpack_info_t *info, int fix_file_name);
int ota_unpack_buff(void *buff, ota_unpack_info_t *info, int fix_file_name);
int is_ota_cam_package(const char *file_name);
uint32_t fit_host_byte_order(uint8_t *data, uint8_t size);
#endif
