#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ota_pack.h"
#include "tcrc.h"

#define log(x, args...) printf(x, ##args)

typedef struct _fix_name_
{
    char *key;
    char *format;
    uint8_t matched_count;
} fix_name_t;

fix_name_t fix_name[] = {
    {.key = "DSP", .format = "ota_rom_dsp%d.bin", 0},
    {.key = "MCU", .format = "ota_rom_mcu%d.bin", 0},
    {.key = "WIFI", .format = "ota_rom_wifi%d.bin", 0},
    {.key = NULL, .format = NULL, 0},
};

//将little endian byte order fit current system
uint32_t fit_host_byte_order(uint8_t *data, uint8_t size)
{
    uint32_t ret = 0;

    if (size >= 1)
    {
        ret += data[0];
    }
    if (size >= 2)
    {
        ret += ((uint32_t)data[1]) << 8;
    }
    if (size >= 3)
    {
        ret += ((uint32_t)data[2]) << 16;
    }
    if (size >= 4)
    {
        ret += ((uint32_t)data[3]) << 24;
    }

    return ret;
}

static ssize_t file_get_size(const char *path)
{
    struct stat st;
    off_t size = 0;

    if (!path)
    {
        return -1;
    }

    if (stat(path, &st) < 0)
    {
        log("%s stat error: %s\n", path, strerror(errno));
    }
    else
    {
        size = st.st_size;
    }

    return size;
}

int ota_unpack_buff(void *buff, ota_unpack_info_t *info, int fix_file_name)
{
    ota_unpack_info_t info_tmp = {0};

    if (buff == NULL)
    {
        log("invalid param\n");
        return -1;
    }

    ota_pack_header_t *header = (ota_pack_header_t *)buff;
    uint32_t magic_num = fit_host_byte_order((uint8_t *)&header->magic_num, 4);
    uint32_t body_crc = fit_host_byte_order((uint8_t *)&header->body_crc, 4);
    uint32_t body_size = fit_host_byte_order((uint8_t *)&header->body_size, 4);

    log("magic number: %x\n", magic_num);
    log("body crc: %x\n", body_crc);
    log("body size: %d\n", body_size);
    log("force flag: %x\n", header->force_flag);
    log("main ver: %s\n", header->main_ver);

    // check magic number
    if (magic_num != OTA_PACK_MAGIC_NUM)
    {
        log("package format err: %x\n", magic_num);
        return -1;
    }

    // update info
    info_tmp.force_flag = header->force_flag;
    strncpy(info_tmp.main_ver, header->main_ver, sizeof(info_tmp.main_ver) - 1);

    // abstact iterms and save them on disk
    ota_pack_body_t *body = (ota_pack_body_t *)(buff + sizeof(ota_pack_header_t));

    // check CRC
    uint32_t crc = CRC32((unsigned char *)body, header->body_size);
    if (crc != body_crc)
    {
        log("check crc failed, now %x, file crc %x\n", crc, body_crc);
        return -2;
    }
    else
    {
        log("crc check pass\n");
    }

    char fix_name_buff[32] = {0};
    for (int i = 0; i < OTA_PACK_FILES_MAX_NUM; i++)
    {
        if (body->index[i].size == 0)
        {
            break;
        }

        const char *file_name = NULL;
        file_name = body->index[i].file_name;
        if (fix_file_name)
        {

            for (int j = 0; fix_name[j].key != NULL; j++)
            {
                if (strstr(file_name, fix_name[j].key) != NULL)
                {
                    fix_name[j].matched_count++;
                    snprintf(fix_name_buff, sizeof(fix_name_buff), fix_name[j].format, fix_name[j].matched_count);
                    file_name = fix_name_buff;
                }
            }
        }

        uint32_t file_size = fit_host_byte_order((uint8_t *)&body->index[i].size, 4);
        uint32_t offset = fit_host_byte_order((uint8_t *)&body->index[i].offset, 4);

        char *content = NULL;
        content = (char *)body + offset;

        log("abstract %dth file %s, size %u, read addr %p, max file number is %d\n", i, file_name, file_size, content, OTA_PACK_FILES_MAX_NUM);

        FILE *fp = fopen(file_name, "wb");
        if (!fp)
        {
            log("create file %s failed\n", file_name);
            return -3;
        }

        int wr_len = fwrite(content, 1, file_size, fp);
        if (wr_len != file_size)
        {
            log("write file %s failed\n", file_name);
            return -4;
        }

        fclose(fp);

        // update iterm info
        info_tmp.iterm_info[i].type = fit_host_byte_order((uint8_t *)&body->index[i].type, 4);
        strncpy(info_tmp.iterm_info[i].file_name, body->index[i].file_name, sizeof(info_tmp.iterm_info[i].file_name) - 1);
        strncpy(info_tmp.iterm_info[i].ver, body->index[i].ver, sizeof(info_tmp.iterm_info[i].ver) - 1);
    }

    if (info != NULL)
    {
        memmove(info, &info_tmp, sizeof(info_tmp));
    }

    return 0;
}

int is_ota_cam_package(const char *file_name)
{
    FILE *fp = fopen(file_name, "rb");
    if (fp == NULL)
    {
        return -1;
    }

    uint32_t magic_num = 0;
    int rd_len = fread(&magic_num, 1, sizeof(magic_num), fp);
    if (rd_len != sizeof(magic_num))
    {
        return -2;
    }

    fclose(fp);

    if (fit_host_byte_order((uint8_t *)&magic_num, 4) != OTA_PACK_MAGIC_NUM)
    {
        return -3;
    }

    return 1;
}

int ota_unpack_file(const char *input_file, ota_unpack_info_t *info, int fix_file_name)
{
    // read file to buffer
    int file_size = file_get_size(input_file);
    if (file_size <= 0)
    {
        log("get file size error\n");
        return -1;
    }

    char *buff = (char *)calloc(1, file_size);
    if (!buff)
    {
        log("calloc error\n");
        return -2;
    }

    FILE *fp = fopen(input_file, "rb");
    if (!fp)
    {
        log("open file failed\n");
        return -3;
    }

    int rd_len = fread(buff, 1, file_size, fp);
    if (rd_len != file_size)
    {
        log("read error\n");
        return -4;
    }

    fclose(fp);

    // parse buffer
    ota_unpack_buff(buff, info, 1);

    free(buff);
    buff = NULL;

    return 0;
}