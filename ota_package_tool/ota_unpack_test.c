#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ota_pack.h"
#include "ota_pack.h"

#define log(x, args...) printf(x, ##args)
#define DBG_U(x)                          \
    {                                     \
        printf("%s:\t\t\t\t%u\n", #x, x); \
    }

#define DBG_S(x)                          \
    {                                     \
        printf("%s:\t\t\t\t%s\n", #x, x); \
    }

int main(int argc, char const *argv[])
{
    printf("build time : %s %s\n\n", __DATE__, __TIME__);

    // parse cmd line
    if (argc != 2)
    {
        log("please input one package\n");
        return -1;
    }

    const char *input_file = NULL;
    input_file = argv[1];

    log("input file: %s\n", input_file);

    int package_format = is_ota_cam_package(input_file);
    if (package_format)
    {
        log("package format correct\n");
    }
    else
    {
        log("package format incorrect!\n");
        return -1;
    }
    
    ota_unpack_info_t info = {0};
    ota_unpack_file(input_file, &info, 1);
    DBG_U(info.force_flag);
    DBG_S(info.main_ver);

    for (size_t i = 0; i < OTA_PACK_FILES_MAX_NUM; i++)
    {
        DBG_S(info.iterm_info[i].file_name);
        DBG_S(info.iterm_info[i].ver);
    }

    // finish
    remove(input_file);

    return 0;
}
