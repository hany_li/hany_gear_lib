#ifndef __MY_CRC_H__
#define __MY_CRC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

uint16_t CRC16(unsigned char* buf, int len);
uint32_t CRC32(unsigned char* buf, int len);

#ifdef __cplusplus
}
#endif

#endif

