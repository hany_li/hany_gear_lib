#include <stdlib.h>
#include "ring_buffer.h"

static mem_hooks_t _g_mem_hooks = {
    .malloc_fn = malloc,
    .free_fn = free,
    .calloc_fn = calloc};

void rb_mem_hooks_init(mem_hooks_t *hooks)
{
    if (NULL == hooks)
    {
        /* Reset hooks */
        _g_mem_hooks.malloc_fn = malloc;
        _g_mem_hooks.free_fn = free;
        _g_mem_hooks.calloc_fn = calloc;
        return;
    }

    _g_mem_hooks.malloc_fn = malloc;
    if (hooks->malloc_fn != NULL)
    {
        _g_mem_hooks.malloc_fn = hooks->malloc_fn;
    }

    _g_mem_hooks.free_fn = free;
    if (hooks->free_fn != NULL)
    {
        _g_mem_hooks.free_fn = hooks->free_fn;
    }

    _g_mem_hooks.calloc_fn = calloc;
    if (hooks->calloc_fn != NULL)
    {
        _g_mem_hooks.calloc_fn = hooks->calloc_fn;
    }
}

static void *rb_malloc(size_t size)
{
    return _g_mem_hooks.malloc_fn(size);
}

static void rb_free(void *ptr)
{
    _g_mem_hooks.free_fn(ptr);
}

static void *rb_calloc(size_t nmeb, size_t size)
{
    return _g_mem_hooks.calloc_fn(nmeb, size);
}

#define MAX_Q_SIZE (100)

struct rb_cdt
{
    rb_element_t *data;
    int head;
    int count;
};

rb_adt_t rb_create(void)
{
    rb_adt_t rb;
    rb = (rb_adt_t)rb_malloc(sizeof(struct rb_cdt));
    rb->data = (rb_element_t *)rb_malloc(MAX_Q_SIZE * sizeof(rb_element_t));    //开出所要的所有元素空间
    rb->head = rb->count = 0;
    return rb;
}

void rb_destroy(rb_adt_t rb)
{
    rb_free(rb->data);
    rb_free(rb);
}

bool rb_in(rb_adt_t rb, rb_element_t value)
{
    if (rb_is_full(rb))
    {
        return false;
    }

    int tail = (rb->head + rb->count) % MAX_Q_SIZE;
    rb->data[tail] = value;
    rb->count++;

    return true;
}

bool rb_out(rb_adt_t rb, rb_element_t *p_value)
{
    if (rb_is_empty(rb))
    {
        return false;
    }

    *p_value = rb->data[rb->head];
    rb->head = (rb->head + 1) % MAX_Q_SIZE;
    rb->count--;

    return true;
}

void rb_flush(rb_adt_t rb)
{
    rb->head = 0;
    rb->count = 0;
}

bool rb_is_empty(rb_adt_t rb)
{
    return (rb->count == 0);
}

bool rb_is_full(rb_adt_t rb)
{
    return (rb->count == MAX_Q_SIZE);
}

int rb_get_length(rb_adt_t rb)
{
    return rb->count;
}

rb_element_t rb_get_element(rb_adt_t rb, int index)
{
    if (index < 0 || index >= rb_get_length(rb))
    {
        return NULL;
    }

    return rb->data[(rb->head + index) % MAX_Q_SIZE];
}
