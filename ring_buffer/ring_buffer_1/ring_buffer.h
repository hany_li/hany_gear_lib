#pragma once

#include <stdbool.h>
#include <stdio.h>

typedef struct
{
    void *(*malloc_fn)(size_t size);
    void (*free_fn)(void *ptr);
    void *(*calloc_fn)(size_t nmemb, size_t size);
} mem_hooks_t;

void rb_mem_hooks_init(mem_hooks_t *hooks);

typedef void *rb_element_t;
typedef struct rb_cdt *rb_adt_t;

rb_adt_t rb_create(void);
void rb_destroy(rb_adt_t rb);

bool rb_in(rb_adt_t rb, rb_element_t value);
bool rb_out(rb_adt_t rb, rb_element_t *p_value);
void rb_flush(rb_adt_t rb);

bool rb_is_empty(rb_adt_t rb);
bool rb_is_full(rb_adt_t rb);

int rb_get_length(rb_adt_t rb);
rb_element_t rb_get_element(rb_adt_t rb, int index);


