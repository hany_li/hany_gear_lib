#include <stdlib.h>
#include <string.h>
#include "ring_buffer.h"

static mem_hooks_t _g_mem_hooks = {
    .malloc_fn = malloc,
    .free_fn = free,
    .calloc_fn = calloc};

void rb_mem_hooks_init(mem_hooks_t *hooks)
{
    if (NULL == hooks)
    {
        /* Reset hooks */
        _g_mem_hooks.malloc_fn = malloc;
        _g_mem_hooks.free_fn = free;
        _g_mem_hooks.calloc_fn = calloc;
        return;
    }

    _g_mem_hooks.malloc_fn = malloc;
    if (hooks->malloc_fn != NULL)
    {
        _g_mem_hooks.malloc_fn = hooks->malloc_fn;
    }

    _g_mem_hooks.free_fn = free;
    if (hooks->free_fn != NULL)
    {
        _g_mem_hooks.free_fn = hooks->free_fn;
    }

    _g_mem_hooks.calloc_fn = calloc;
    if (hooks->calloc_fn != NULL)
    {
        _g_mem_hooks.calloc_fn = hooks->calloc_fn;
    }
}

static void *rb_malloc(size_t size)
{
    return _g_mem_hooks.malloc_fn(size);
}

static void rb_free(void *ptr)
{
    _g_mem_hooks.free_fn(ptr);
}

static void *rb_calloc(size_t nmeb, size_t size)
{
    return _g_mem_hooks.calloc_fn(nmeb, size);
}

#define MAX_Q_SIZE (100)

struct rb_cdt
{
    char *data;
    size_t head;
    size_t count;
};

static size_t _g_max_count;    //队列中最大的元素数量
static size_t _g_element_size; //每个元素的大小

rb_adt_t rb_create(const size_t max_element_count, const size_t element_size)
{
    rb_adt_t rb;

    rb = (rb_adt_t)rb_malloc(sizeof(struct rb_cdt));
    rb->data = (char *)rb_malloc(max_element_count * element_size);
    memset(rb->data, 0, max_element_count * _g_element_size);
    rb->head = rb->count = 0;

    _g_max_count = max_element_count;
    _g_element_size = element_size;

    return rb;
}

void rb_destroy(rb_adt_t rb)
{
    rb_free(rb->data);
    rb_free(rb);

    _g_max_count = 0;
    _g_element_size = 0;
}

bool rb_in(rb_adt_t rb, const void *value)
{
    if (rb_is_full(rb))
    {
        return false;
    }

    int tail = (rb->head + rb->count) % _g_max_count;
    int offset = tail * _g_element_size;
    memmove(&(rb->data[offset]), value, _g_element_size);
    rb->count++;

    return true;
}

bool rb_out(rb_adt_t rb, void *p_value)
{
    if (rb_is_empty(rb))
    {
        return false;
    }
    int offset = rb->head * _g_element_size;
    memmove(p_value, &(rb->data[offset]), _g_element_size);
    rb->head = (rb->head + 1) % _g_max_count;
    rb->count--;

    return true;
}

void rb_flush(rb_adt_t rb)
{
    rb->head = 0;
    rb->count = 0;
    memset(rb->data, 0, _g_max_count * _g_element_size);
}

bool rb_is_empty(rb_adt_t rb)
{
    return (rb->count == 0);
}

bool rb_is_full(rb_adt_t rb)
{
    return (rb->count == _g_max_count);
}

int rb_get_length(rb_adt_t rb)
{
    return rb->count;
}

void *rb_get_element(rb_adt_t rb, const size_t index)
{
    if (index < 0 || index >= rb_get_length(rb))
    {
        return NULL;
    }

    size_t data_index = (rb->head + index) % _g_max_count;
    size_t data_offset = data_index * _g_element_size;

    return (&rb->data[data_offset]);
}
