#pragma once

#include <stdbool.h>
#include <stdio.h>

typedef struct
{
    void *(*malloc_fn)(size_t size);
    void (*free_fn)(void *ptr);
    void *(*calloc_fn)(size_t nmemb, size_t size);
} mem_hooks_t;

void rb_mem_hooks_init(mem_hooks_t *hooks);

typedef struct rb_cdt *rb_adt_t;

rb_adt_t rb_create(size_t max_element_count, size_t element_size);
void rb_destroy(rb_adt_t rb);

bool rb_in(rb_adt_t rb, const void *value);
bool rb_out(rb_adt_t rb, void *p_value);
void rb_flush(rb_adt_t rb);

bool rb_is_empty(rb_adt_t rb);
bool rb_is_full(rb_adt_t rb);

int rb_get_length(rb_adt_t rb);
void *rb_get_element(rb_adt_t rb, const size_t index);
