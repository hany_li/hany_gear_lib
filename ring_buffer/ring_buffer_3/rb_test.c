#include <stdio.h>
#include "ring_buffer.h"
#include <stdlib.h>

void rb_dump_1(rb_adt_t rb)
{
    if (rb_is_empty(rb))
    {
        printf("\nring buffer empty\n");
        return;
    }

    for (int i = 0; i < rb_get_length(rb); i++)
    {
        printf("%d ", *(char *)rb_get_element(rb, i));
    }
}

void ring_test_1(void)
{
    char buf[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    const size_t max_count = sizeof(buf) / sizeof(buf[0]);

    printf("%s begin...!\n", __FUNCTION__);

    //create ring buffer
    struct rb_cdt rb1;

    rb_adt_t rb = rb_create(&rb1, max_count, sizeof(buf[0]));

    //put data in
    for (int i = 0; i < max_count; i++)
    {
        rb_in(rb, &buf[i]);
    }

    //dump ring buffer
    rb_dump_1(rb);

    for (int i = 0; i < 5; i++)
    {
        void *element;
        //take data out
        rb_out(rb, &element);
        //dump ring buffer
        rb_dump_1(rb);
    }

    char dummy = 55;
    rb_in(rb, &dummy);
    rb_dump_1(rb);

    char dummy2 = 66;
    rb_in(rb, &dummy2);
    rb_dump_1(rb);

    rb_flush(rb);
    rb_dump_1(rb);

    dummy = 11;
    rb_in(rb, &dummy);
    rb_dump_1(rb);

    dummy = 22;
    rb_in(rb, &dummy);
    rb_dump_1(rb);

    //destroy ring buffer
    rb_destroy(rb);
    printf("%s done!\n", __FUNCTION__);
    printf("////////////////////////////////////////////////////////////////\n\n");
}

/////////////事件队列////////////////////

/**
 * 这个就是事件触发之后，将其处理函数与其参数入队，等系统有时间了，再逐一出队处理。
 * 本列中是使用malloc分配入队元素空间的，在实际项目应用中，应提前开僻好元素空间，以减少系统长时间运行后的内存碎片
 */
#if 1
#define event_queue_create() rb_create()
#define event_queue_destroy() rb_destroy()

struct queue_event
{
    void (*event_fn)(void *p_arg);
    void *p_arg;
};

#include <assert.h>
void event_queue_in(rb_adt_t rb, void (*event)(void *p_arg), void *p_arg)
{
    struct queue_event x;
    assert(rb);
    assert(event);

    x.event_fn = event; //具体操作函数指针
    x.p_arg = p_arg;    //操作函数的参数
    rb_in(rb, &x);
}

#include <time.h>
void time_up(void *p_arg) //具体的操作函数
{
    printf("time up...%d\n", *((int *)p_arg));
}

void event_check(rb_adt_t rb)
{
    assert(rb);

    static time_t old;
    static time_t now;
    now = time(NULL);

    /* 时间超时1s, 就入队超时处理程序，依当前情况界入合适的参数*/
    if (now - old > 1)
    {
        old = now;
        event_queue_in(rb, time_up, &now);
    }
}

/**
 * @brief 队列不空，就一个人地出队，并执行相关的带参函数
 * @param rb
 * @return (void)
 */
void do_event(rb_adt_t rb)
{
    struct queue_event x;

    assert(rb);
    if (!rb_is_empty(rb))
    {
        rb_out(rb, &x);
        x.event_fn(x.p_arg);
    }
}

#include <unistd.h>
void ring_test_2(void)
{
    rb_adt_t rb;
    const size_t max_count = 2;

    printf("%s begin...!\n", __FUNCTION__);
    struct rb_cdt rb2;
    rb = rb_create(&rb2, max_count, sizeof(struct queue_event));

    for (size_t i = 0; i < 5; i++)
    {
        printf("------------%ld th-----------\n", i);
        event_check(rb);
        do_event(rb);
        sleep(1);
    }

    rb_destroy(rb);

    printf("%s done!\n", __FUNCTION__);
    printf("////////////////////////////////////////////////////////////////\n\n");
}
#endif

int main(int argc, char **argv)
{
    ring_test_1();
    ring_test_2();

    return 0;
}