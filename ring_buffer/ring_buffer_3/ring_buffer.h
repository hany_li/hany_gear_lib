#pragma once

#include <stdbool.h>
#include <stdio.h>

#define RB_BUFF_SIZE (1024)
typedef struct rb_cdt *rb_adt_t;
struct rb_cdt
{
    char data[RB_BUFF_SIZE];
    size_t head;
    size_t count;
};

rb_adt_t rb_create(struct rb_cdt * rb_instance, size_t max_element_count, size_t element_size);
void rb_destroy(rb_adt_t rb);

bool rb_in(rb_adt_t rb, const void *value);
bool rb_out(rb_adt_t rb, void *p_value);
void rb_flush(rb_adt_t rb);

bool rb_is_empty(rb_adt_t rb);
bool rb_is_full(rb_adt_t rb);

int rb_get_length(rb_adt_t rb);
void *rb_get_element(rb_adt_t rb, const size_t index);
