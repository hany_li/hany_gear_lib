#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define LOCAL_TEST
#ifdef LOCAL_TEST
#define SERVER_IP "127.0.0.1" 
#else
#define SERVER_IP "192.168.137.83" //按实际情况修改
#endif
#define SERVER_PORT 8000		   //按实际情况修改

/**
 * argv[0]:rx enable
 * argv[1]:send numbers
*/
int main(int argc, char *argv[])
{
	int client_sockfd;
	unsigned int len;
	struct sockaddr_in remote_addr;
	char buf[BUFSIZ];
	memset(&remote_addr, 0, sizeof(remote_addr));
	remote_addr.sin_family = AF_INET;

	remote_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	remote_addr.sin_port = htons(SERVER_PORT);

	if (argc != 3)
	{
		printf("Usage: client rx_enable send_count\n");
		return 0;
	}

	unsigned int i = 0;
	int rx_enable = atoi(argv[1]); // rx
	unsigned int max_count = 0;	   //count

	int dummy = atoi(argv[2]);
	if (dummy == 0)
	{
		max_count = 0xFFFFFFFF;
	}
	else
	{
		max_count = dummy;
	}

	if ((client_sockfd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("socket");
		return 1;
	}

	if (connect(client_sockfd, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr)) < 0)
	{
		perror("connect");
		return 1;
	}
	printf("connected to server\n");

	while (1)
	{
		// scanf("%s", buf);
		snprintf(buf, 10, "%d", i++);
		len = send(client_sockfd, buf, strlen(buf), 0);
		if (len == -1)
		{
			printf("Send failure...\n");
			break;
		}
		else
		{
			buf[len] = '\0';
			printf("Send:%s\n", buf);
		}

		if (rx_enable)
		{
			len = recv(client_sockfd, buf, BUFSIZ, 0);
			if (len == -1)
			{
				printf("Receive failure...\n");
				break;
			}
			else
			{
				buf[len] = '\0';
				printf("Received:%s\n", buf);
			}
		}

		usleep(100 * 1000);
		if (i == max_count)
		{
			break;
		}
	}
	printf("test done!\n");
	close(client_sockfd);
	return 0;
}
